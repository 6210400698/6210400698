# Sparkie condominium management
``` bash
6210400698
    |   .gitignore
    |   README.md
    |   pom.xml
    |   
    +---SparkieCondominiumManagement (Main program folder.)
    |   |   6210400698.jar
    |   |   6210400698.pdf
    |   |   6210400698-UML.png
    |   |   
    |   +---data (CSV folder for data from login program.)
    |   |       Account.csv
    |   |       ItemData.csv
    |   |       RoomData.csv
    |   |       
    |   \---lib (Library that used in program.)
    |           bootstrapfx-core-0.2.4.jar
    |           
    +---data (CSV folder for data before install program.)
    |       Account.csv
    |       ItemData.csv
    |       RoomData.csv
    |       
    +---src (Code and file)
    |   \---login
    |       +---java
    |       |   +---account (Package for account class.)
    |       |   |       Account.java
    |       |   |       AccountList.java
    |       |   |       AdminAccount.java
    |       |   |       HostAccount.java
    |       |   |       HostList.java
    |       |   |       UserAccount.java
    |       |   |       UserList.java
    |       |   |       
    |       |   +---admin (Package for admin page.)
    |       |   |       AdminChangePasswordController.java
    |       |   |       AdminMainPageController.java
    |       |   |       HostListPageController.java
    |       |   |       ManageHostPageController.java
    |       |   |       
    |       |   +---data (Package for read/write data.)
    |       |   |       AccountDataInfo.java
    |       |   |       AccountFileAccountDataInfo.java
    |       |   |       ItemDataInfo.java
    |       |   |       ItemFileDataInfo.java
    |       |   |       RoomDataInfo.java
    |       |   |       RoomFileDataInfo.java
    |       |   |       
    |       |   +---host (Package for host page.)
    |       |   |       HistoryPageController.java
    |       |   |       HostChangePasswordController.java
    |       |   |       HostMainPageController.java
    |       |   |       ItemListController.java
    |       |   |       ManageItemPageController.java
    |       |   |       ManageRoomController.java
    |       |   |       ManageUserPageController.java
    |       |   |       RoomListController.java
    |       |   |       
    |       |   +---item (Package for item class.)
    |       |   |       Document.java
    |       |   |       Item.java
    |       |   |       ItemList.java
    |       |   |       Letter.java
    |       |   |       Parcel.java
    |       |   |       
    |       |   +---login (Package for launch program and login page.)
    |       |   |       InfoPageController.java
    |       |   |       Launcher.java
    |       |   |       LoginController.java
    |       |   |       ProfileController.java
    |       |   |       
    |       |   +---room (Package for room class.)
    |       |   |       Room.java
    |       |   |       RoomList.java
    |       |   |       
    |       |   \---user (Package for user page.)
    |       |           UserChangePasswordController.java
    |       |           UserMainPageController.java
    |       |           UserReceiveHistoryController.java
    |       |           UserReceiveItemController.java
    |       |           UserRegisterController.java
    |       |           
    |       \---resources (FXML file and Image)
    |           |   adminchangepassword.fxml
    |           |   adminmainpage.fxml
    |           |   historylist.fxml
    |           |   hostchangepassword.fxml
    |           |   hostlistpage.fxml
    |           |   hostmainpage.fxml
    |           |   infopage.fxml
    |           |   itemlist.fxml
    |           |   loginpage.fxml
    |           |   managehostpage.fxml
    |           |   manageitempage.fxml
    |           |   manageroom.fxml
    |           |   manageuserpage.fxml
    |           |   profile.fxml
    |           |   roomlist.fxml
    |           |   userchangepassword.fxml
    |           |   usermainpage.fxml
    |           |   userreceivehistory.fxml
    |           |   usercheckitem.fxml
    |           |   userregister.fxml
    |           |   
    |           \---image (Image in program and style.css)
    |                   CondoManual.png
    |                   CondoStructure.png
    |                   CreatorIcon.png
    |                   EmptyBackground.jpg
    |                   ProfilePicture.jpg
    |                   background.jpg
    |                   otherpage.jpg
    |                   style.css
    |                   tableviewStyle.css
    |                   document.png
    |                   letter.png
    |                   parcel.png
```

# How to run program.

- Go to 6210400698\SparkieCondominiumManagement\
- Double click 6210400698.jar to run program.

# Progress
2020-11-21 - 2020-11-24(Fix project)
- 
- Edit project UML diagram.
- Edit 6210400698.pdf
- Add AccountList, HostList, UserList and RoomList.
- Add Label to every text field and Combo Box.
- Add hint to manage user page and manage room page.
- Add user id to fix duplicate name and use id to add item instead of name and username.
- Use user id based instead of name and username to add to room and receive item.
- Add search by id function in UserList.
- Rename back button to log out button.
- Separate object class and list of object class out.
- Add Combo Box for easier use.
- Remove receive item button in user page and transfer it to host page(User must come and receive from host only).
- Host can change item to received when user come and get item.
- Add user id label in user page.
- Rename userreceiveitem.fxml to usercheckitem.fxml.
- Add arrange room function to make room number create in order.


2020-10-25 - 2020-10-31
- 
- Add item image to project.
- Add comment to program.
- Edit variable name.

2020-10-17 - 2020-10-24
- 
- Update pdf file.
- Edit README.md.
- Add UML diagram to project.
- Add search box in host item list page.
- Add select receive item feature.
- Add user item history.
- Remove unnecessary method.

2020-10-09 - 2020-10-16
-
- Add history page to host item page.
- Add manual page.
- Add isValid function to check format.
- Add regex to check username and password.
- Edit README.md
- Edit file name.
- Edit sort function in TableView.

2020‑10‑01 - 2020‑10‑08
-
- Add read/write file to project.
- Add RoomData.csv file.
- Add hostList TableView.
- Add Letter TableView.
- Add ItemData.csv
- Add User receive item list.
- Edit user receive item page.
- Edit read/write file function.

2020‑09‑21 - 2020‑09‑28
-
- Add maven to project.
- Add user interface.
- Add Manage item function.
- Add remove user from room function.
- Add polymorphism.
- Add check if username is already existed method.
- Add add and receive item function.
- Add user register page.
- Add Profile page.
- Add manage room feature.
- Add checkExist and isExist method in Data class.
