package admin;

import account.*;
import item.ItemList;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import login.*;
import room.RoomList;

import java.io.IOException;

public class HostListPageController
{
    @FXML private TableView<HostAccount> hostTable;
    private ObservableList<HostAccount> hostListTable;

    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountList accountsList;
    private ItemList itemsList;
    public void setValue(ItemList itemsList,AccountList accountsList,RoomList roomsList,AdminAccount adminAccount,HostList hostsList,UserList usersList)
    {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }
    @FXML private Label currentAccountLabel;
    @FXML public void initialize()
    {
        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                showList();
            }
        });
    }
    public void handleAdminBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button adminBackBtn = (Button) event.getSource();
        Stage stage = (Stage) adminBackBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleManageHostBtnOnAction(ActionEvent event) throws IOException
    {
        Button manageHostBtn = (Button) event.getSource();
        Stage stage = (Stage) manageHostBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/managehostpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageHostPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleHostListBtnOnAction(ActionEvent event) throws IOException
    {
        Button hostListBtn = (Button) event.getSource();
        Stage stage = (Stage) hostListBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostlistpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostListPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        AdminChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void showList()
    {
        hostListTable = FXCollections.observableArrayList((hostsList.getHostAccounts()));
        hostTable.setItems(hostListTable);
        TableColumn name = new TableColumn("Name");
        TableColumn username = new TableColumn("Username");
        TableColumn status = new TableColumn("Status");
        TableColumn attempt = new TableColumn("Login attempt");
        TableColumn latestLogin = new TableColumn("Latest login");

        name.setCellValueFactory( new PropertyValueFactory<HostAccount,String>("name"));
        username.setCellValueFactory( new PropertyValueFactory<HostAccount,String>("username"));
        status.setCellValueFactory( new PropertyValueFactory<HostAccount,String>("status"));
        attempt.setCellValueFactory( new PropertyValueFactory<HostAccount,String>("attempt"));
        latestLogin.setCellValueFactory(new PropertyValueFactory<HostAccount,String>("latestLogin"));

        latestLogin.setSortType(TableColumn.SortType.DESCENDING);
        hostTable.getColumns().addAll(name,username,status,attempt,latestLogin);
        hostTable.getSortOrder().add(latestLogin);
    }

}
