package admin;

import account.*;
import data.AccountFileAccountDataInfo;
import data.AccountDataInfo;
import item.Item;
import item.ItemList;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import login.*;
import room.Room;
import room.RoomList;

import java.io.IOException;

public class ManageHostPageController {
    @FXML
    TextField usernameBox, nameBox, blockBox, unblockBox;
    @FXML
    PasswordField passwordBox, confirmPasswordBox;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountDataInfo source;
    private Account accounts;
    private AccountList accountsList;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    Alert alert;
    @FXML
    private Label currentAccountLabel;

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
            }
        });
        alert = new Alert(Alert.AlertType.NONE);
        accounts = new Account();
    }

    public void handleAddBtnOnAction(ActionEvent event) throws IOException {
        if (nameBox.getText().equals("") || usernameBox.getText().equals("")
                || passwordBox.getText().equals("") || confirmPasswordBox.getText().equals("")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        } else {
            try {
                Double.parseDouble(nameBox.getText());
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Name can't be a number.");
                alert.show();
            } catch (NumberFormatException e) {
                if (!accountsList.isExist(usernameBox.getText())) {
                    if (accounts.isValidName(nameBox.getText()) && accounts.isValidPassword(passwordBox.getText()) && !usernameBox.getText().contains(",")) {
                        if (passwordBox.getText().equals(confirmPasswordBox.getText())) {
                            HostAccount temp = new HostAccount(usernameBox.getText(), passwordBox.getText(), nameBox.getText(), "Not block", 0, "-");
                            hostsList.addAccount(temp);
                            accountsList.addAccount(temp);
                            source = new AccountFileAccountDataInfo("data", "Account.csv");
                            source.setAccountData(accountsList);
                            alert.setAlertType(Alert.AlertType.INFORMATION);
                            alert.setContentText("Add host successful.");
                            alert.show();
                            Button addBtn = (Button) event.getSource();
                            Stage stage = (Stage) addBtn.getScene().getWindow();
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostlistpage.fxml"));
                            stage.setScene(new Scene(loader.load(), 1024, 768));
                            HostListPageController account = loader.getController();
                            account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                            stage.show();
                        } else {
                            alert.setAlertType(Alert.AlertType.INFORMATION);
                            alert.setContentText("Password doesn't match.");
                            alert.show();
                        }
                    } else if (!accounts.isValidPassword(passwordBox.getText())) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Password is invalid.");
                        alert.show();
                    } else if (!accounts.isValidName(nameBox.getText())) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Name is invalid.");
                        alert.show();
                    } else if (usernameBox.getText().contains(",")) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Can't have , in username.");
                        alert.show();
                    }
                } else {
                    alert.setAlertType(Alert.AlertType.INFORMATION);
                    alert.setContentText("Username is already used.");
                    alert.show();
                }
            }
        }
    }

    public void handleBlockBtnOnAction() {
        if (blockBox.getText().equals("")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        } else {
            for (Account acc : accountsList.getAccounts()) {
                if (acc.getUsername().equals(blockBox.getText()) && ((HostAccount) acc).getStatus().equals("Not block")) {
                    ((HostAccount) acc).setStatus("Block");
                    source = new AccountFileAccountDataInfo("data", "Account.csv");
                    source.setAccountData(accountsList);
                    alert.setAlertType(Alert.AlertType.INFORMATION);
                    alert.setContentText("Block successful.");
                    alert.show();
                    break;
                } else if (acc.getUsername().equals(blockBox.getText()) && ((HostAccount) acc).getStatus().equals("Block")) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("This username is already blocked.");
                    alert.show();
                    break;
                } else {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Username is incorrect.");
                    alert.show();
                }
            }
        }
    }

    public void handleUnblockBtnOnAction() {
        if (unblockBox.getText().equals("")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        } else {
            for (Account acc : accountsList.getAccounts()) {
                if (acc.getUsername().equals(unblockBox.getText()) && ((HostAccount) acc).getStatus().equals("Block")) {
                    ((HostAccount) acc).setStatus("Not block");
                    source = new AccountFileAccountDataInfo("data", "Account.csv");
                    source.setAccountData(accountsList);
                    alert.setAlertType(Alert.AlertType.INFORMATION);
                    alert.setContentText("Unblock successful.");
                    alert.show();
                    break;
                } else if (acc.getUsername().equals(unblockBox.getText()) && ((HostAccount) acc).getStatus().equals("Not block")) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("This username is not blocked.");
                    alert.show();
                    break;
                } else {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Username is incorrect.");
                    alert.show();
                }
            }
        }
    }

    public void handleManageHostBtnOnAction(ActionEvent event) throws IOException {
        Button manageHostBtn = (Button) event.getSource();
        Stage stage = (Stage) manageHostBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/managehostpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageHostPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleHostListBtnOnAction(ActionEvent event) throws IOException {
        Button hostListBtn = (Button) event.getSource();
        Stage stage = (Stage) hostListBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostlistpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostListPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        AdminChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
}
