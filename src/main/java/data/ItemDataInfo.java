package data;

import item.ItemList;

public interface ItemDataInfo
{
    ItemList getItemData();

    void setItemData(ItemList item);
}
