package data;

import item.*;

import java.io.*;

public class ItemFileDataInfo implements ItemDataInfo
{
    private String fileDirectoryName;
    private String fileName;
    private ItemList itemsList;

    public ItemFileDataInfo(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException
    {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null)
        {
            String[] data = line.split(",");
            if(data[0].equals("Document"))
            {
                Document doc = new Document(data[0],data[1],data[2],Integer.parseInt(data[3]),data[4],data[5],data[10].trim());
                doc.setDateIn(data[6]);
                doc.setDateOut(data[8]);
                doc.setHostReceive(data[7]);
                doc.setUserReceive(data[9]);
                itemsList.addItem(doc);
            }
            else if(data[0].equals("Letter"))
            {
                Letter let = new Letter(data[0],data[1],data[2],Integer.parseInt(data[3]),data[4],data[9].trim());
                let.setDateIn(data[5]);
                let.setDateOut(data[7]);
                let.setHostReceive(data[6]);
                let.setUserReceive(data[8]);
                itemsList.addItem(let);
            }
            else if(data[0].equals("Parcel"))
            {
                Parcel par = new Parcel(data[0],data[1],data[2],Integer.parseInt(data[3]),data[4],data[5],data[6],data[11].trim());
                par.setDateIn(data[7]);
                par.setDateOut(data[9]);
                par.setHostReceive(data[8]);
                par.setUserReceive(data[10]);
                itemsList.addItem(par);
            }
        }
        reader.close();
    }

    @Override
    public ItemList getItemData()
    {
        try
        {
            itemsList = new ItemList();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return itemsList;
    }

    @Override
    public void setItemData(ItemList itemsList)
    {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Item all: itemsList.getItems())
            {
                if(all instanceof Document)
                {
                    String line = "Document"+ "," + all.getSenderName() +
                            "," + all.getReceiverName() + "," + all.getRoomNumber() +
                            "," + all.getSize() + "," + ((Document) all).getClassification()+
                            "," + all.getDateIn() + "," + all.getHostReceive() + "," + all.getDateOut()+
                            "," + all.getUserReceive()+","+ all.getId();
                    writer.append(line);
                }
                else if(all instanceof Letter)
                {
                    String line = "Letter" + "," + all.getSenderName() + "," + all.getReceiverName() + "," + all.getRoomNumber() + "," + all.getSize()+
                            "," + all.getDateIn() + "," + all.getHostReceive() + "," + all.getDateOut()+
                            "," + all.getUserReceive()+","+ all.getId();
                    writer.append(line);
                }
                else if(all instanceof Parcel)
                {
                    String line = "Parcel" + "," + all.getSenderName() + "," + all.getReceiverName() + "," + all.getRoomNumber()+","+ all.getSize() + "," + ((Parcel) all).getDeliveryService() + "," + ((Parcel) all).getTrackNumber()+
                            "," + all.getDateIn() + "," + all.getHostReceive() + "," + all.getDateOut()+
                            "," + all.getUserReceive()+","+ all.getId();
                    writer.append(line);
                }
                writer.newLine();
            }
            for (Item history: itemsList.getHistory())
            {
                if(history instanceof Document)
                {
                    String line = "Document"+ "," + history.getSenderName() +
                            "," + history.getReceiverName() + "," + history.getRoomNumber() +
                            "," + history.getSize() + "," + ((Document) history).getClassification()+
                            "," + history.getDateIn() + "," + history.getHostReceive() + "," + history.getDateOut()+
                            "," + history.getUserReceive() +","+ history.getId();
                    writer.append(line);
                }
                else if(history instanceof Letter)
                {
                    String line = "Letter" + "," + history.getSenderName() + "," + history.getReceiverName() + "," + history.getRoomNumber() + "," + history.getSize()+
                            "," + history.getDateIn() + "," + history.getHostReceive() + "," + history.getDateOut()+
                            "," + history.getUserReceive() +","+ history.getId();;
                    writer.append(line);
                }
                else if(history instanceof Parcel)
                {
                    String line = "Parcel" + "," + history.getSenderName() + "," + history.getReceiverName() + "," + history.getRoomNumber()+","+ history.getSize() + "," + ((Parcel) history).getDeliveryService() + "," + ((Parcel) history).getTrackNumber()+
                            "," + history.getDateIn() + "," + history.getHostReceive() + "," + history.getDateOut()+
                            "," + history.getUserReceive() +","+ history.getId();;
                    writer.append(line);
                }
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
