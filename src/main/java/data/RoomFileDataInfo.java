package data;

import room.Room;
import account.*;
import room.RoomList;

import java.io.*;

public class RoomFileDataInfo implements RoomDataInfo
{

    private String fileDirectoryName;
    private String fileName;
    private RoomList roomsList;

    public RoomFileDataInfo(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted()
    {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException
    {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null)
        {
            String[] data = line.split(",");
            char[] split = data[1].toCharArray();
            char floor = split[1];
            char building = split[0];

            if(data[0].equals("4"))
            {
                roomsList.addRoom(new Room(Integer.parseInt(data[1]),Integer.parseInt(data[0]),"Double room",building,floor));
            }
            else if(data[0].equals("2"))
            {
                roomsList.addRoom(new Room(Integer.parseInt(data[1]),Integer.parseInt(data[0]),"Single room",building,floor));
            }
        }
        reader.close();
    }

    @Override
    public RoomList getRoomsList()
    {
        try
        {
            roomsList = new RoomList();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return roomsList;
    }

    public void setRoomData(RoomList roomsList)
    {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Room someRoom: roomsList.getAllRooms())
            {
                    String line = String.valueOf(someRoom.getAmount())+","+someRoom.getRoomNumber();
                    String customer = "";
                    for(UserAccount acc: someRoom.getCustomers())
                        customer += acc.getName() + ",";
                    line = line + "," + customer;
                    writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
