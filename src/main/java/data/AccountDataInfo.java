package data;

import account.AccountList;

public interface AccountDataInfo
{
    AccountList getAccountData();

    void setAccountData(AccountList acc);
}
