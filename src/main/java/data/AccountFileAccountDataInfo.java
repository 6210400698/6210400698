package data;

import account.*;

import java.io.*;

public class AccountFileAccountDataInfo implements AccountDataInfo
{

    private String fileDirectoryName;
    private String fileName;
    private AccountList accountsList;

    public AccountFileAccountDataInfo(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException
    {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null)
        {
            String[] data = line.split(",");
            if(data[0].equals("Admin"))
            {
                accountsList.addAccount(new AdminAccount(data[1],data[2],data[3]));
            }
            else if(data[0].equals("Host"))
            {
                accountsList.addAccount(new HostAccount(data[1],data[2],data[3],data[5],Integer.parseInt(data[4]),data[6]));
            }
            else if(data[0].equals("User"))
            {
                accountsList.addAccount(new UserAccount(data[1],data[2],data[3],Integer.parseInt(data[4]),data[5]));
            }
        }
        reader.close();
    }

    @Override
    public AccountList getAccountData()
    {
        try
        {
            accountsList = new AccountList();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return accountsList;
    }

    @Override
    public void setAccountData(AccountList acc)
    {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Account account: acc.getAccounts())
            {
                if(account instanceof AdminAccount)
                {
                    String line = "Admin"+ "," + account.getUsername() + "," + account.getPassword() + "," + account.getName();
                    writer.append(line);
                }
                else if(account instanceof HostAccount)
                {
                    String line = "Host" + "," + account.getUsername() + "," + account.getPassword() + "," + account.getName() + "," + String.valueOf(((HostAccount) account).getAttempt()) + "," + String.valueOf(((HostAccount) account).getStatus()) + "," + ((HostAccount) account).getLatestLogin();
                    writer.append(line);
                }
                else if(account instanceof UserAccount)
                {
                    String line = "User" + "," + account.getUsername() + "," + account.getPassword() + "," + account.getName()+","+ ((UserAccount)account).getRoom()+","+((UserAccount)account).getId();
                    writer.append(line);
                }
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
