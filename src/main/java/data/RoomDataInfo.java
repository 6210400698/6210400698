package data;

import room.RoomList;

public interface RoomDataInfo
{
    RoomList getRoomsList();

    void setRoomData(RoomList roomsList);
}
