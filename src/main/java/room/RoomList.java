package room;

import account.UserAccount;

import java.util.ArrayList;
import java.util.Collections;

public class RoomList
{
    private ArrayList<Room> allRooms;

    public RoomList()
    {
        allRooms = new ArrayList<>();
    }

    /**
     *
     * @param room add room to allRooms list and sort.
     */
    public void addRoom(Room room)
    {
        allRooms.add(room);
        Collections.sort(allRooms);
    }

    public ArrayList<Room> getAllRooms() { return allRooms; }

    /**
     *
     * @param roomNumber check this room number to see if room is full or not.
     * @return false if room is not full and true if room is full.
     */
    public boolean checkFull(int roomNumber)
    {
        for(Room room: allRooms)
            if(room.getRoomNumber() == roomNumber)
                if(room.getCustomers().size() < room.getAmount())
                    return false;
        return true;
    }

    /**
     *
     * @param id remove user with this id from room.
     * @return return true if successfully remove and false if not.
     */
    public boolean removeFromRoom(String id)
    {
        for (Room allRoom : allRooms)
        {
            for (int j = 0; j < allRoom.getCustomers().size(); j++)
            {
                if (allRoom.getCustomers().get(j).getId().equals(id))
                {
                    allRoom.getCustomers().remove(j);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * @param roomNumber check to see if room number is exist or not.
     * @return true if room is already existed and false if not.
     */
    public boolean checkExist(int roomNumber)
    {
        for (Room room : allRooms)
        {
            if (room.getRoomNumber() == roomNumber)
            {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param room check to see if room is full or not.
     * @return return true if room is not full and return false if full.
     */
    public boolean checkFull(Room room)
    {
        if(room.getCustomers().size() < room.getAmount())
            return true;
        return false;
    }

    /**
     * Check to see if room number is arrange in order.
     * @param roomNumber room number of the user request.
     * @return return room number if input room number is in order and return last inorder room number that have to create
     * first inorder to create new room.
     */
    public int arrangeRoom(int roomNumber)
    {
        int lastRoom = 0;
          for(Room room: allRooms)
          {
              if(room.getRoomNumber() <= 189 && roomNumber <= 189)
              {
                  lastRoom = room.getRoomNumber();
                  if (room.getRoomNumber() + 1 == roomNumber)
                      return roomNumber;
              }
              else if(room.getRoomNumber() <= 289 && room.getRoomNumber() >= 200 && roomNumber <= 289 && roomNumber >= 200)
              {
                  lastRoom = room.getRoomNumber();
                  if (room.getRoomNumber() + 1 == roomNumber)
                      return roomNumber;
              }
              else if(room.getRoomNumber() <= 389 && room.getRoomNumber() >= 300 && roomNumber <= 389 && roomNumber >= 300)
              {
                  lastRoom = room.getRoomNumber();
                  if (room.getRoomNumber() + 1 == roomNumber)
                      return roomNumber;
              }
              else if(room.getRoomNumber() <= 489 && room.getRoomNumber() >= 400 && roomNumber <= 489 && roomNumber >= 400)
              {
                  lastRoom = room.getRoomNumber();
                  if (room.getRoomNumber() + 1 == roomNumber)
                      return roomNumber;
              }
          }
      return lastRoom+1;
    }

    /**
     *
     * @param user user account that will be added to a room.
     * @param room room number that will be used to find a room.
     * @return true if add to room successful and false if not success.
     */
    public boolean addToRoom(UserAccount user,int room)
    {
        for(Room check: allRooms)
            if(check.getCustomers().size() < check.getAmount() && check.getRoomNumber() == room)
            {
                check.addCustomer(user);
                return true;
            }
        return false;
    }

}
