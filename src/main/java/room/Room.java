package room;

import account.UserAccount;

import java.util.ArrayList;
import java.util.Collections;

public class Room implements Comparable<Room>
{
    private int roomNumber;
    private int amount;
    private String size;
    private char building;
    private char floor;
    private ArrayList<UserAccount> customers;
    public Room(){}
    public Room(int roomNumber, int amount, String size, char building, char floor)
    {
        this.roomNumber = roomNumber;
        this.amount = amount;
        this.size = size;
        this.building = building;
        this.floor = floor;
        customers = new ArrayList<>();
    }

    public int getRoomNumber() { return roomNumber; }
    public int getAmount() {
        return amount;
    }
    public ArrayList<UserAccount> getCustomers() { return customers; }
    public String getSize() {
        return size;
    }
    public void addCustomer(UserAccount user)
    {
        customers.add(user);
    }

    @Override
    public String toString()
    {
        return "Room number: " + roomNumber + " Customer: "+ getCustomers() + " Amount: " + amount;
    }
    public char getBuilding() {
        return building;
    }

    public char getFloor() { return floor; }
    /**
     * Check to see if room number exceed room number limit or not.
     *
     * @param  roomNumber   input room number to be checked.
     * @return              true if room is valid and false if not.
     */
    public boolean isValid(int roomNumber)
    {
        return (roomNumber >= 100 && roomNumber <= 189) || (roomNumber >= 200 && roomNumber <= 289) ||
                (roomNumber >= 300 && roomNumber <= 389) || (roomNumber >= 400 && roomNumber <= 489);
    }

    @Override
    public int compareTo(Room room)
    {
        if(room.getRoomNumber() < roomNumber)
            return 1;
        else if(room.getRoomNumber() > roomNumber)
            return -1;
        else
            return 0;
    }
}
