package item;

import account.UserAccount;

import java.util.ArrayList;

public class ItemList
{
    private ArrayList<Item> items;
    private ArrayList<Item> history;

    public ItemList()
    {
        items = new ArrayList<>();
        history = new ArrayList<>();
    }

    /**
     * Function to remove item that's already received by user.
     */
    public void removeItem()
    {
        for(Item item: new ArrayList<>(items))
            if(!item.getDateOut().equals("-"))
                items.remove(item);
    }

    /**
     *
     * @param item item that will be added to history.
     */
    public void addHistory(Item item)
    {
        history.add(item);
    }

    /**
     *
     * @param item item that will be added to items.
     */
    public void addItem(Item item){ items.add(item); }

    /**
     * Add item to history and remove item that already received from items.
     *
     * @param user UserAccount that receive item.
     * @param outDate date and time that user receive item.
     * @param item item that user receive.
     * @return true if user receive successful.
     *
     */
    public boolean receiveItem(UserAccount user, String outDate, Item item)
    {
        if(user.getRoom() != -1)
        {
            item.setUserReceive(user.getName());
            item.setDateOut(outDate);
            item.setRoomNumber(user.getRoom());
            history.add(item);
            items.remove(item);
            return true;
        }
        return false;
    }

    public ArrayList<Item> getHistory() {
        return history;
    }

    public ArrayList<Item> getItems() {
        return items;
    }
}
