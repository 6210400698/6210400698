package item;

public class Item
{
    private String senderName;
    private String receiverName;
    private int roomNumber;
    private String size;
    private String dateIn;
    private String dateOut;
    private String userReceive;
    private String hostReceive;
    private String type;
    private String id;
    public Item () { }
    public Item(String type, String senderName, String receiverName, int roomNumber, String size, String id)
    {
        this.type = type;
        this.senderName = senderName;
        this.receiverName = receiverName;
        this.roomNumber = roomNumber;
        this.size = size;
        this.id = id;
        dateIn = "-";
        dateOut = "-";
        hostReceive =  "-";
        userReceive = "-";
    }

    public void setDateIn(String dateIn) { this.dateIn = dateIn; }
    public void setDateOut(String dateOut) { this.dateOut = dateOut; }
    public void setUserReceive(String userReceive) { this.userReceive = userReceive; }
    public void setHostReceive(String hostReceive) { this.hostReceive = hostReceive; }

    public void setRoomNumber(int roomNumber) {this.roomNumber = roomNumber; }

    public String getId() { return id; }
    public String getDateIn() {
        return dateIn;
    }
    public String getDateOut() {
        return dateOut;
    }
    public String getUserReceive() {
        return userReceive;
    }
    public String getHostReceive() {
        return hostReceive;
    }
    public String getSenderName() { return senderName; }
    public String getReceiverName() { return receiverName; }
    public int getRoomNumber() { return roomNumber; }
    public String getSize() { return size; }
    public String getType() { return type; }

}
