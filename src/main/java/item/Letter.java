package item;

public class Letter extends Item
{

    public Letter(String type, String senderName, String receiverName, int roomNumber, String size, String id)
    {
        super(type, senderName, receiverName, roomNumber, size, id);
    }

    @Override
    public String toString() { return "Letter From: "+getSenderName()+" To: "+getReceiverName()+" Room number: "+getRoomNumber()+ " Size: "+getSize()+ " (Host)Receive by: "+getHostReceive() +" Date in: "+getDateIn() + " (User)Receive by: "+getUserReceive()+" Date out: "+getDateOut(); }

}
