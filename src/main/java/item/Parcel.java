package item;

public class Parcel extends Item
{
    private String deliveryService;
    private String trackNumber;
    public Parcel(String type, String senderName, String receiverName, int roomNumber, String size, String deliveryService, String trackNumber, String id)
    {
        super(type, senderName, receiverName, roomNumber, size, id);
        this.deliveryService = deliveryService;
        this.trackNumber = trackNumber;
    }

    public String getDeliveryService() { return deliveryService; }
    public String getTrackNumber() { return trackNumber; }

    @Override
    public String toString() { return "Parcel From: "+getSenderName()+" To: "+getReceiverName()+" Room number: "+getRoomNumber()+" Delivery service: "+getDeliveryService()+" Tracking number: "+getTrackNumber()+ " Size: "+getSize()+ " (Host)Receive by: "+getHostReceive() +" Date in: "+getDateIn() + " (User)Receive by: "+getUserReceive()+" Date out: "+getDateOut(); }
}
