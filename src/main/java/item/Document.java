package item;


public class Document extends Item
{
    private String classification;
    public Document(String type, String senderName, String receiverName, int roomNumber, String size, String classification, String id)
    {
        super(type, senderName, receiverName, roomNumber, size, id);
        this.classification = classification;
    }
    public String getClassification() { return classification; }

    @Override
    public String toString() { return "Document From: "+getSenderName()+" To: "+getReceiverName()+" Room number: " +getRoomNumber()+" Classification: " +getClassification()+ " Size: "+getSize()
            + " (Host)Receive by: "+getHostReceive() +" Date in: "+getDateIn() + " (User)Receive by: "+getUserReceive()+" Date out: "+getDateOut(); }
}
