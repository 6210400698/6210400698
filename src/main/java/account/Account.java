package account;

public class Account
{
    private String username;
    private String password;
    private String name;
    public Account(){}
    public Account(String username, String password, String name)
    {
        this.username = username;
        this.password = password;
        this.name = name;
    }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public String getName() { return name; }
    public void setPassword(String password) { this.password = password; }

    /**
     * Check name format.
     *
     * @param  name   input name
     * @return        true if name match regex or correct format and false if not match.
     */

    public boolean isValidName(String name)
    {
        return ((name != null)
                && (!name.equals(""))
                && (name.matches("^[a-zA-Z]+(?:[\\s.]+[a-zA-Z]+)*$")));
    }

    /**
     * Check password format.
     *
     * @param  password input password.
     * @return         true if password match regex or correct format and false if not match.
     */
    public boolean isValidPassword(String password)
    {
        return ((password != null)
                && (!password.equals(""))
                && (password.matches("^[a-zA-Z0-9]+$")));
    }

    @Override
    public String toString() {
        return "Username: "+username+" Password: "+password;}
}
