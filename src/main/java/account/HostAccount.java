package account;

public class HostAccount extends Account
{
    private int attempt;
    private String status;
    private String latestLogin;

    public HostAccount(String username, String password, String name, String status, int attempt, String latestLogin)
    {
        super(username, password, name);
        this.status = status;
        this.attempt = attempt;
        this.latestLogin = latestLogin;
    }

    public String getStatus() { return status; }
    /**
     * Set host account status to block or not block. If set back to not block attempt will reset to 0.
     *
     * @param  status  the status that will turn host in to.
     */
    public void setStatus(String status)
    {
        if(status.equals("Not block"))
        {
            this.status = status;
            attempt = 0;
        }
        else if(status.equals("Block"))
        {
            this.status = status;
        }
    }

    public int getAttempt() { return attempt; }
    public String getLatestLogin() { return latestLogin; }

    public void setAttempt(int attempt) { this.attempt = attempt; }
    public void setLatestLogin(String latestLogin) { this.latestLogin = latestLogin; }

    @Override
    public String toString() { return "Host: "+getUsername()+" Password: "+getPassword()+" Status: "+getStatus()+" Attempt: "+getAttempt()+" Latest login: "+getLatestLogin();}

}
