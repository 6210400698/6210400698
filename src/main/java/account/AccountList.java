package account;

import java.util.ArrayList;

public class AccountList
{
    private Account currentAccount;
    private ArrayList<Account> accounts;
    public AccountList(){ accounts = new ArrayList<>();}
    public ArrayList<Account> getAccounts() { return accounts; }
    public void addAccount(Account acc) { accounts.add(acc); }


    /**
     *
     * @param username username that will be check in accounts if username exist or not.
     * @param password password that will be check in accounts if password exist or not.
     * @return true and set currentAccount or return null if not found.
     */
    public boolean checkUsernamePassword(String username, String password)
    {
        for(Account acc: accounts)
        {
            if(acc.getUsername().equals(username) && acc.getPassword().equals(password))
            {
                currentAccount = acc;
                return true;
            }
        }
        currentAccount = null;
        return false;
    }

    /**
     *
     * @param username username that will be check that is it already used.
     * @return true if username is already used and false if not already used.
     */
    public boolean isExist(String username)
    {
        for(Account acc: accounts)
        {
            if(acc.getUsername().equals(username))
            {
                return true;
            }
        }
        return false;
    }
    public Account getCurrentAccount() { return currentAccount; }

}
