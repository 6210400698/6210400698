package account;

import java.util.ArrayList;
import java.util.Collections;

public class UserList
{
    private ArrayList<UserAccount> userAccounts;
    public UserList()
    {
        userAccounts = new ArrayList<>();
    }
    public void addAccount(UserAccount acc)
    {
        userAccounts.add(acc);
    }

    /**
     * sort userAccounts according to room number from least to most.
     */
    public void sortUsersList()
    {
        Collections.sort(userAccounts);
    }

    /**
     * Function to create unique id for each user in the condo.
     * @return id that is created according to number of user in the condo.
     */
    public String createId()
    {
        String id = "";
        if(userAccounts.size() < 10)
            id = "00"+userAccounts.size();
        else if(userAccounts.size() < 100)
            id = "0"+userAccounts.size();
        else if(userAccounts.size() < 1000)
            id = String.valueOf(userAccounts.size());
        else if(userAccounts.size() < 10000)
            id = String.valueOf(userAccounts.size());
        return id;
    }

    /**
     *
     * @param username username that will be checked if username is already assigned by host.
     * @param password password that will be checked if password is not assigned.
     * @return true if username is already assigned and password is not assigned and return false if password
     *         is already assigned or username is not assigned by host.
     */
    public boolean registerUser(String username, String password)
    {
        for(UserAccount user: userAccounts)
            if(user.getUsername().equals(username) && user.getPassword().equals("not assigned"))
            {
                user.setPassword(password);
                return true;
            }
        return false;
    }

    /**
     *
     * @param id id of the user that will be searched for an user account.
     * @return user account if found and return null if not found.
     */
    public UserAccount searchById(String id)
    {
        for(UserAccount user: userAccounts)
        {
            if (user.getId().equals(id))
            {
                return user;
            }
        }
        return null;
    }

    public ArrayList<UserAccount> getUserAccounts()
    {
        return userAccounts;
    }

}
