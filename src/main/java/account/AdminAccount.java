package account;

public class AdminAccount extends Account
{
    public AdminAccount(String username, String password, String name)
    {
        super(username, password, name);
    }
    @Override
    public String toString() { return "Admin: "+getUsername()+" Password: "+getPassword();}
}
