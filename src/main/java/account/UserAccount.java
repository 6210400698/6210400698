package account;

import item.Item;

import java.util.ArrayList;
import java.util.Collections;

public class UserAccount extends Account implements Comparable<UserAccount>
{
    private ArrayList<Item> userItem;
    private ArrayList<Item> userInbox;
    private int room;
    private String id;
    public UserAccount(String username, String password, String name,int room, String id)
    {
        super(username, password, name);
        userItem = new ArrayList<>();
        userInbox = new ArrayList<>();
        this.room = room;
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public int getRoom() { return room; }
    public ArrayList<Item> getUserInbox() {
        return userInbox;
    }
    public ArrayList<Item> getUserItem() { return userItem; }

    public void setRoom(int room)
    {
        this.room = room;
    }
    /**
     * Move item from userInbox to userItem after user received item.
     *
     * @param  item   item that will be removed from userInbox and add to userItem.
     */
    public void removeReceived(Item item)
    {
        userItem.add(item);
        userInbox.remove(item);
    }

    /**
     *
     * @param item item that will be checked in user inbox.
     * @param user user to get inbox to check item.
     * @return true if item is the same as in user inbox.
     */
    public boolean checkSameItem(Item item, UserAccount user)
    {
        int check = 0;
        for(Item i: user.getUserInbox())
        {
            if(i.equals(item))
            {
                check = 1;
            }
        }
        if(check == 1)
            return true;
        else
            return false;
    }

    @Override
    public String toString() { return getName()+ " " + getUsername()+ " "+ getId();}

    @Override
    public int compareTo(UserAccount user) {
        if(user.getRoom() < room)
            return 1;
        else if(user.getRoom() > room)
            return -1;
        else
            return 0;
    }
}