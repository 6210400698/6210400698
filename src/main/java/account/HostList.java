package account;

import java.util.ArrayList;

public class HostList
{
    private ArrayList<HostAccount> hostAccounts;
    public HostList()
    {
        hostAccounts = new ArrayList<>();
    }

    /**
     *
     * @param acc Host account that will be added to hostAccounts.
     */
    public void addAccount(HostAccount acc)
    {
        hostAccounts.add(acc);
    }
    public ArrayList<HostAccount> getHostAccounts() { return hostAccounts; }


}
