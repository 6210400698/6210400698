package user;

import account.*;
import item.Document;
import item.Item;
import item.ItemList;
import item.Parcel;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import login.LoginController;
import room.RoomList;

import java.io.IOException;

public class UserReceiveHistoryController {
    @FXML
    private TableView<Item> historyTable;
    private ObservableList<Item> historyListTable;

    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountList accountsList;
    private ItemList itemsList;
    @FXML
    private Label currentAccountLabel, currentRoomLabel, idLabel;
    @FXML
    private Label inByLabel, senderLabel, receiverLabel, sizeLabel, classificationLabel, deliveryLabel, trackLabel;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                idLabel.setText("ID: "+((UserAccount)(accountsList).getCurrentAccount()).getId());
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                if (!((UserAccount) (accountsList.getCurrentAccount())).getUserItem().isEmpty())
                    showList();
                if (((UserAccount) accountsList.getCurrentAccount()).getRoom() == -1)
                    currentRoomLabel.setText("Not in a room.");
                else
                    currentRoomLabel.setText("Room number: " + String.valueOf(((UserAccount) accountsList.getCurrentAccount()).getRoom()));
                historyTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                {
                    if (newValue != null) {
                        showSelectedItem(newValue);
                    }
                });
            }
        });
    }

    public void showSelectedItem(Item item) {
        inByLabel.setText("In by: " + item.getHostReceive());
        sizeLabel.setText("Size: " + item.getSize());
        senderLabel.setText("Sender: " + item.getSenderName());
        receiverLabel.setText("Receiver: " + item.getReceiverName());
        classificationLabel.setText("");
        deliveryLabel.setText("");
        trackLabel.setText("");
        if (item instanceof Document) {
            classificationLabel.setText("Classification: " + ((Document) item).getClassification());
            deliveryLabel.setText("");
        } else if (item instanceof Parcel) {
            deliveryLabel.setText("Delivery service: " + ((Parcel) item).getDeliveryService());
            trackLabel.setText("Tracking number: " + ((Parcel) item).getTrackNumber());
        }
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleCheckBtnOnAction(ActionEvent event) throws IOException {
        Button checkBtn = (Button) event.getSource();
        Stage stage = (Stage) checkBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/usercheckitem.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserReceiveItemController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void showList() {
        historyListTable = FXCollections.observableArrayList(((UserAccount) (accountsList.getCurrentAccount())).getUserItem());
        historyTable.setItems(historyListTable);
        TableColumn type = new TableColumn("Type");
        TableColumn room = new TableColumn("Room");
        TableColumn out = new TableColumn("Date out");
        TableColumn outBy = new TableColumn("Out by");

        type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
        room.setCellValueFactory(new PropertyValueFactory<Item, String>("roomNumber"));
        out.setCellValueFactory(new PropertyValueFactory<Item, String>("dateOut"));
        outBy.setCellValueFactory(new PropertyValueFactory<Item, String>("userReceive"));
        outBy.setPrefWidth(140);

        historyTable.getColumns().addAll(type, room, out, outBy);
    }

}
