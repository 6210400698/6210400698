package user;

import account.*;
import item.ItemList;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import login.LoginController;
import room.RoomList;

import java.io.IOException;

public class UserMainPageController {
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountList accountsList;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML
    private Label currentAccountLabel, currentRoomLabel, idLabel;

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run()
            {
                idLabel.setText("ID: "+((UserAccount)(accountsList).getCurrentAccount()).getId());
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                if (((UserAccount) accountsList.getCurrentAccount()).getRoom() == -1)
                    currentRoomLabel.setText("Not in a room.");
                else
                    currentRoomLabel.setText("Room number: " + String.valueOf(((UserAccount) accountsList.getCurrentAccount()).getRoom()));
            }
        });
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleCheckBtnOnAction(ActionEvent event) throws IOException {
        Button checkBtn = (Button) event.getSource();
        Stage stage = (Stage) checkBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/usercheckitem.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserReceiveItemController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
}
