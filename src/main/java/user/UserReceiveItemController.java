package user;

import account.*;
import item.Document;
import item.Item;
import item.ItemList;
import item.Parcel;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import login.LoginController;
import room.RoomList;

import java.io.IOException;

public class UserReceiveItemController {
    @FXML
    private TableView<Item> inboxTable;
    private ObservableList<Item> inboxListTable;

    @FXML
    private Label inByLabel, senderLabel, receiverLabel, sizeLabel, classificationLabel, deliveryLabel, trackLabel, idLabel;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountList accountsList;
    private Item selectedItem;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML
    private Label currentRoomLabel, currentAccountLabel;

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run()
            {
                itemsList.removeItem();
                idLabel.setText("ID: "+((UserAccount)(accountsList).getCurrentAccount()).getId());
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                if (((UserAccount) accountsList.getCurrentAccount()).getRoom() == -1)
                    currentRoomLabel.setText("Not in a room.");
                else
                    currentRoomLabel.setText("Room number: " + String.valueOf(((UserAccount) accountsList.getCurrentAccount()).getRoom()));
                if (!((UserAccount) (accountsList.getCurrentAccount())).getUserInbox().isEmpty())
                    showList();

                inboxTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                {
                    if (newValue != null) {
                        selectedItem = newValue;
                        showSelectedItem(selectedItem);
                    }
                });
            }
        });
    }

    public void showSelectedItem(Item item) {
        inByLabel.setText("In by: " + item.getHostReceive());
        sizeLabel.setText("Size: " + item.getSize());
        senderLabel.setText("Sender: " + item.getSenderName());
        receiverLabel.setText("Receiver: " + item.getReceiverName());
        classificationLabel.setText("");
        deliveryLabel.setText("");
        trackLabel.setText("");
        if (item instanceof Document) {
            classificationLabel.setText("Classification: " + ((Document) item).getClassification());
            deliveryLabel.setText("");
        } else if (item instanceof Parcel) {
            deliveryLabel.setText("Delivery service: " + ((Parcel) item).getDeliveryService());
            trackLabel.setText("Tracking number: " + ((Parcel) item).getTrackNumber());
        }
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleCheckBtnOnAction(ActionEvent event) throws IOException {
        Button checkBtn = (Button) event.getSource();
        Stage stage = (Stage) checkBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/usercheckitem.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserReceiveItemController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleHistoryBtnOnAction(ActionEvent event) throws IOException {
        Button historyBtn = (Button) event.getSource();
        Stage stage = (Stage) historyBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userreceivehistory.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserReceiveHistoryController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void showList() {
        inboxListTable = FXCollections.observableArrayList(((UserAccount) (accountsList.getCurrentAccount())).getUserInbox());
        inboxTable.setItems(inboxListTable);
        TableColumn type = new TableColumn("Type");
        TableColumn size = new TableColumn("Size");
        TableColumn in = new TableColumn("Date in");
        TableColumn inBy = new TableColumn("In by");

        type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
        size.setCellValueFactory(new PropertyValueFactory<Item, String>("size"));
        in.setCellValueFactory(new PropertyValueFactory<Item, String>("dateIn"));
        inBy.setCellValueFactory(new PropertyValueFactory<Item, String>("hostReceive"));
        inBy.setPrefWidth(120);

        in.setSortType(TableColumn.SortType.DESCENDING);
        inboxTable.getColumns().addAll(type, size, in, inBy);
        inboxTable.getSortOrder().add(in);
    }
}
