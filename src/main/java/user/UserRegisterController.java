package user;

import account.*;
import data.AccountFileAccountDataInfo;
import data.AccountDataInfo;
import item.Item;
import item.ItemList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import login.LoginController;
import room.Room;
import room.RoomList;

import java.io.IOException;

public class UserRegisterController {
    @FXML
    private TextField usernameBox, passwordBox, confirmPasswordBox;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountDataInfo source;
    private Account accounts;
    private AccountList accountsList;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    private Alert alert;

    @FXML
    public void initialize() {
        alert = new Alert(Alert.AlertType.NONE);
        accounts = new Account();
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleRegisterBtnOnAction(ActionEvent event) throws IOException {
        if (!accounts.isValidPassword(passwordBox.getText()) || !accounts.isValidPassword(confirmPasswordBox.getText())) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Password is invalid.");
            alert.show();
        } else if (!accountsList.isExist(usernameBox.getText())) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Username isn't correct.");
            alert.show();
        } else if (!passwordBox.getText().equals(confirmPasswordBox.getText())) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Password doesn't match.");
            alert.show();
        } else if (passwordBox.getText().equals(confirmPasswordBox.getText()) && accounts.isValidPassword(passwordBox.getText())) {
            if (usersList.registerUser(usernameBox.getText(), passwordBox.getText())) {
                source = new AccountFileAccountDataInfo("data", "Account.csv");
                source.setAccountData(accountsList);
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Register successful.");
                alert.show();
                Button registerBtn = (Button) event.getSource();
                Stage stage = (Stage) registerBtn.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
                stage.setScene(new Scene(loader.load(), 1024, 768));
                LoginController account = loader.getController();
                account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                stage.show();
            } else {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Already registered.");
                alert.show();
            }
        }
    }
}
