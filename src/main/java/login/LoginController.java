package login;

import account.*;
import admin.AdminMainPageController;
import data.*;
import host.HostMainPageController;
import item.Item;
import item.ItemList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import room.Room;
import room.RoomList;
import user.UserMainPageController;
import user.UserRegisterController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LoginController {
    @FXML
    TextField usernameBox;
    @FXML
    PasswordField passwordBox;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountDataInfo source;
    private RoomDataInfo roomSource;
    private ItemDataInfo itemSource;
    private AccountList accountsList;
    private ItemList itemsList;
    private Account accounts;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    private Alert alert;

    public void initialize() {
        accounts = new Account();
        itemsList = new ItemList();
        accountsList = new AccountList();
        hostsList = new HostList();
        usersList = new UserList();
        roomsList = new RoomList();
        alert = new Alert(Alert.AlertType.NONE);
        itemSource = new ItemFileDataInfo("data", "ItemData.csv");
        roomSource = new RoomFileDataInfo("data", "RoomData.csv");
        source = new AccountFileAccountDataInfo("data", "Account.csv");
        itemsList = itemSource.getItemData();
        roomsList = roomSource.getRoomsList();
        accountsList = source.getAccountData();
        if(!roomsList.checkExist(100)&&!roomsList.checkExist(200)
                &&!roomsList.checkExist(300)&&!roomsList.checkExist(400))
        {
            roomsList.addRoom(new Room(100, 2, "Single room", '1', '0'));
            roomsList.addRoom(new Room(200, 2, "Single room", '2', '0'));
            roomsList.addRoom(new Room(300, 2, "Single room", '3', '0'));
            roomsList.addRoom(new Room(400, 2, "Single room", '4', '0'));
            roomSource.setRoomData(roomsList);
        }
        if (!accountsList.isExist("admin"))
        {
            accountsList.addAccount(new AdminAccount("admin", "admin", "Teeraphat Sutprasoet"));
            accountsList.addAccount(new HostAccount("host", "host", "HostAccount TestLogin", "Not block", 0, "-"));
            accountsList.addAccount(new UserAccount("user", "user", "UserAccount TestLogin", -1,"000"));
        }
        for (Account acc : accountsList.getAccounts()) {
            if (acc instanceof HostAccount)
                hostsList.addAccount((HostAccount) acc);
            else if (acc instanceof UserAccount)
                usersList.addAccount((UserAccount) acc);
        }
        for (Account acc : accountsList.getAccounts()) {
            if (acc instanceof UserAccount) {
                usersList.searchById(((UserAccount)acc).getId()).setRoom(((UserAccount) acc).getRoom());
                roomsList.addToRoom((UserAccount) acc, ((UserAccount) acc).getRoom());
            }
        }
        for (Item item : itemsList.getItems()) {
            if (!item.getDateOut().equals("-")) {
                itemsList.addHistory(item);
                usersList.searchById(item.getId()).getUserItem().add(item);
            } else if (item.getDateOut().equals("-"))
            {
                usersList.searchById(item.getId()).getUserInbox().add(item);
            }
        }
        source.setAccountData(accountsList);
    }

    public void handleLoginBtnOnAction(ActionEvent event) throws IOException, NullPointerException {
        if (accountsList.checkUsernamePassword(usernameBox.getText(), passwordBox.getText()) && accountsList.getCurrentAccount() instanceof AdminAccount) {
            Button loginBtn = (Button) event.getSource();
            Stage stage = (Stage) loginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminmainpage.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            AdminMainPageController account = loader.getController();
            account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
            stage.show();
        } else if (accountsList.checkUsernamePassword(usernameBox.getText(), passwordBox.getText()) && accountsList.getCurrentAccount() instanceof HostAccount && ((HostAccount) (accountsList.getCurrentAccount())).getStatus().equals("Not block")) {
            String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(Calendar.getInstance().getTime());
            ((HostAccount) accountsList.getCurrentAccount()).setLatestLogin(timeStamp);
            source.setAccountData(accountsList);
            Button loginBtn = (Button) event.getSource();
            Stage stage = (Stage) loginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostmainpage.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            HostMainPageController account = loader.getController();
            account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
            stage.show();
        } else if (accountsList.checkUsernamePassword(usernameBox.getText(), passwordBox.getText()) && accountsList.getCurrentAccount() instanceof HostAccount && ((HostAccount) (accountsList.getCurrentAccount())).getStatus().equals("Block")) {
            ((HostAccount) accountsList.getCurrentAccount()).setAttempt(((HostAccount) accountsList.getCurrentAccount()).getAttempt() + 1);
            source = new AccountFileAccountDataInfo("data", "Account.csv");
            source.setAccountData(accountsList);
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Your account is blocked. Please contact admin for more info.");
            alert.show();
        } else if (accountsList.checkUsernamePassword(usernameBox.getText(), "not assigned")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("You have to register first.");
            alert.show();
        } else if (accountsList.checkUsernamePassword(usernameBox.getText(), passwordBox.getText()) && !accountsList.getCurrentAccount().getPassword().equals("not assigned") && accountsList.getCurrentAccount() instanceof UserAccount) {
            Button loginBtn = (Button) event.getSource();
            Stage stage = (Stage) loginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/usermainpage.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            UserMainPageController account = loader.getController();
            account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
            stage.show();
        } else {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Username or password is incorrect.");
            alert.show();
        }
    }

    public void handleRegisterBtnOnAction(ActionEvent event) throws IOException {
        Button registerBtn = (Button) event.getSource();
        Stage stage = (Stage) registerBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userregister.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserRegisterController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleProfileBtnOnAction(ActionEvent event) throws IOException {
        Button profileBtn = (Button) event.getSource();
        Stage stage = (Stage) profileBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/profile.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ProfileController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleInfoBtnOnAction(ActionEvent event) throws IOException {
        Button infoBtn = (Button) event.getSource();
        Stage stage = (Stage) infoBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/infopage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        InfoPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
}
