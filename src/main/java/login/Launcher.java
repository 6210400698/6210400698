package login;

import account.Account;
import account.AdminAccount;
import account.HostAccount;
import account.UserAccount;
import data.AccountDataInfo;
import data.ItemDataInfo;
import data.RoomDataInfo;
import item.Document;
import item.Item;
import item.Letter;
import item.Parcel;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import room.Room;

public class Launcher extends Application
{
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    private Room roomAll;
    private Parcel parcel;
    private Letter letters;
    private Document documents;
    private AccountDataInfo source;
    private RoomDataInfo roomSource;
    private ItemDataInfo itemSource;
    private Account accounts;
    private Item items;
    public void setValue(Item items, Account accounts, Parcel parcel, Letter letters, Document documents, Room roomAll, AdminAccount adminAccount, HostAccount hostAccount, UserAccount userAccount)
    {
        this.items = items;
        this.accounts = accounts;
        this.parcel = parcel;
        this.letters = letters;
        this.documents = documents;
        this.roomAll = roomAll;
        this.adminAccount = adminAccount;
        this.hostAccount = hostAccount;
        this.userAccount = userAccount;
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("/loginpage.fxml"));
        primaryStage.setTitle("Sparkie condominium management");
        primaryStage.setScene(new Scene(root, 1024, 768));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
