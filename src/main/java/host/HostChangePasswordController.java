package host;

import account.*;
import data.AccountFileAccountDataInfo;
import data.AccountDataInfo;
import item.ItemList;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import login.*;
import room.RoomList;

import java.io.IOException;

public class HostChangePasswordController {
    Alert alert;
    @FXML
    private Label currentAccountLabel;

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
            }
        });
        alert = new Alert(Alert.AlertType.NONE);
        accounts = new Account();
    }

    @FXML
    private PasswordField oldPasswordBox, newPasswordBox, newPasswordConfirmBox;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountDataInfo source;
    private Account accounts;
    private AccountList accountsList;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    public void handleChangeBtnOnAction(ActionEvent event) throws IOException {
        if (oldPasswordBox.getText().equals("") || newPasswordBox.getText().equals("") || newPasswordConfirmBox.getText().equals("")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        } else {
            if (accountsList.getCurrentAccount().getPassword().equals(oldPasswordBox.getText())
                    && newPasswordBox.getText().equals(newPasswordConfirmBox.getText())
                    && !oldPasswordBox.getText().equals(newPasswordBox.getText()) && accounts.isValidPassword(newPasswordBox.getText())) {
                accountsList.getCurrentAccount().setPassword(newPasswordBox.getText());
                source = new AccountFileAccountDataInfo("data", "Account.csv");
                source.setAccountData(accountsList);
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Change password successful.");
                alert.show();
                Button changeBtn = (Button) event.getSource();
                Stage stage = (Stage) changeBtn.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
                stage.setScene(new Scene(loader.load(), 1024, 768));
                LoginController account = loader.getController();
                account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                stage.show();
            } else if (accountsList.getCurrentAccount().getPassword().equals(newPasswordBox.getText())
                    && newPasswordBox.getText().equals(newPasswordConfirmBox.getText())) {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("New password can't be old password.");
                alert.show();
                oldPasswordBox.clear();
                newPasswordBox.clear();
                newPasswordConfirmBox.clear();
            } else if (accountsList.getCurrentAccount().getPassword().equals(newPasswordBox.getText())
                    && !newPasswordBox.getText().equals(newPasswordConfirmBox.getText())) {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Password doesn't match.");
                alert.show();
                oldPasswordBox.clear();
                newPasswordBox.clear();
                newPasswordConfirmBox.clear();
            } else if (!accounts.isValidPassword(newPasswordBox.getText()) || !accounts.isValidPassword(newPasswordConfirmBox.getText())) {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Password is invalid.");
                alert.show();
                oldPasswordBox.clear();
                newPasswordBox.clear();
                newPasswordConfirmBox.clear();
            } else if (!accountsList.getCurrentAccount().getPassword().equals(oldPasswordBox.getText())) {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Password isn't correct.");
                alert.show();
                oldPasswordBox.clear();
                newPasswordBox.clear();
                newPasswordConfirmBox.clear();
            } else {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Password isn't correct.");
                alert.show();
                oldPasswordBox.clear();
                newPasswordBox.clear();
                newPasswordConfirmBox.clear();
            }
        }
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException {
        Button manageUserBtn = (Button) event.getSource();
        Stage stage = (Stage) manageUserBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException {
        Button manageRoomBtn = (Button) event.getSource();
        Stage stage = (Stage) manageRoomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageItemBtnOnAction(ActionEvent event) throws IOException {
        Button manageItemBtn = (Button) event.getSource();
        Stage stage = (Stage) manageItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageitempage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageItemPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleCheckItemBtnOnAction(ActionEvent event) throws IOException {
        Button checkItemBtn = (Button) event.getSource();
        Stage stage = (Stage) checkItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ItemListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleRoomBtnOnAction(ActionEvent event) throws IOException {
        Button roomBtn = (Button) event.getSource();
        Stage stage = (Stage) roomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        RoomListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
}
