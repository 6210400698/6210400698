package host;

import account.*;
import data.AccountFileAccountDataInfo;
import data.AccountDataInfo;
import data.RoomDataInfo;
import data.RoomFileDataInfo;
import item.Item;
import item.ItemList;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import login.*;
import room.Room;
import room.RoomList;


import java.io.IOException;

public class ManageUserPageController
{   @FXML private TextField nameBox, usernameBox;
    @FXML private Label currentAccountLabel;
    @FXML private ComboBox<String> userComboBox;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountDataInfo source;
    private Account accounts;
    private AccountList accountsList;
    private RoomDataInfo roomSource;
    private ItemList itemsList;
    public void setValue(ItemList itemsList,AccountList accountsList,RoomList roomsList,AdminAccount adminAccount,HostList hostsList,UserList usersList)
    {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }
    private Alert alert;
    @FXML public void initialize()
    {
        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                usersList.sortUsersList();
                for(UserAccount u: usersList.getUserAccounts())
                    if(u.getRoom() != -1)
                        userComboBox.getItems().add(u.getName()+" - "+u.getId() +" - " + u.getRoom());
            }
        });
        alert = new Alert(Alert.AlertType.NONE);
        roomsList = new RoomList();
        accounts = new Account();
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleAddBtnOnAction(ActionEvent event) throws IOException
    {
        if(nameBox.getText().equals("") || usernameBox.getText().equals(""))
        {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        }
        else
            {
            try {
                Integer.parseInt(nameBox.getText());
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Name can't be a number.");
                alert.show();
            } catch (NumberFormatException e) {
                if (accounts.isValidName(nameBox.getText()))
                {
                    if (!accountsList.isExist(usernameBox.getText()) && !usernameBox.getText().contains(","))
                    {
                            UserAccount temp = new UserAccount(usernameBox.getText(), "not assigned", nameBox.getText(), -1, usersList.createId());
                            accountsList.addAccount(temp);
                            usersList.addAccount(temp);
                            roomSource = new RoomFileDataInfo("data", "RoomData.csv");
                            roomSource.setRoomData(roomsList);
                            source = new AccountFileAccountDataInfo("data", "Account.csv");
                            source.setAccountData(accountsList);
                            alert.setAlertType(Alert.AlertType.INFORMATION);
                            alert.setContentText("Add user successful.");
                            alert.show();
                            Button addBtn = (Button) event.getSource();
                            Stage stage = (Stage) addBtn.getScene().getWindow();
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostmainpage.fxml"));
                            stage.setScene(new Scene(loader.load(), 1024, 768));
                            HostMainPageController account = loader.getController();
                            account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                            stage.show();
                    }
                    else if(usernameBox.getText().contains(","))
                    {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Can't have , in username.");
                        alert.show();
                    }
                    else
                        {
                        alert.setAlertType(Alert.AlertType.INFORMATION);
                        alert.setContentText("Username is already used.");
                        alert.show();
                    }
                } else {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Name is invalid.");
                    alert.show();
                }
            }
        }
    }
    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException
    {
        Button manageUserBtn = (Button) event.getSource();
        Stage stage = (Stage) manageUserBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException
    {
        Button manageRoomBtn = (Button) event.getSource();
        Stage stage = (Stage) manageRoomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleManageItemBtnOnAction(ActionEvent event) throws IOException
    {
        Button manageItemBtn = (Button) event.getSource();
        Stage stage = (Stage) manageItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageitempage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageItemPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleCheckItemBtnOnAction(ActionEvent event) throws IOException
    {
        Button checkItemBtn = (Button) event.getSource();
        Stage stage = (Stage) checkItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ItemListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleRemoveBtnOnAction(ActionEvent event) throws IOException {
        try {
            String[] username = userComboBox.getValue().split("-");
            final Alert dlg = new Alert(Alert.AlertType.CONFIRMATION);
            dlg.setContentText("Are you sure you want to remove this user?");
            if (dlg.showAndWait().get() == ButtonType.OK) {
                if (roomsList.removeFromRoom(username[1].trim())) {
                    usersList.searchById(username[1].trim()).setRoom(-1);
                    roomSource = new RoomFileDataInfo("data", "RoomData.csv");
                    roomSource.setRoomData(roomsList);
                    source = new AccountFileAccountDataInfo("data", "Account.csv");
                    source.setAccountData(accountsList);
                    alert.setAlertType(Alert.AlertType.INFORMATION);
                    alert.setContentText("Remove successful");
                    Button RemoveBtn = (Button) event.getSource();
                    Stage stage = (Stage) RemoveBtn.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
                    stage.setScene(new Scene(loader.load(), 1024, 768));
                    RoomListController account = loader.getController();
                    account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                    stage.show();
                }
                else {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("This user is not in a room.");
                }
                alert.show();
            }
        }
        catch(NullPointerException e)
        {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please select user account.");
            alert.show();
        }
        }
    public void handleRoomBtnOnAction(ActionEvent event) throws IOException
    {
        Button roomBtn = (Button) event.getSource();
        Stage stage = (Stage) roomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        RoomListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
}
