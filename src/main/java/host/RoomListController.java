package host;

import account.*;
import item.Item;
import item.ItemList;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import login.LoginController;
import room.Room;
import room.RoomList;

import java.io.IOException;


public class RoomListController {
    @FXML
    private TableView<Room> roomTable;
    private ObservableList<Room> roomListTable;

    @FXML
    private Label currentAccountLabel, firstCustomerLabel, secondCustomerLabel, thirdCustomerLabel, fourthCustomerLabel;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountList accountsList;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                if (!roomsList.getAllRooms().isEmpty())
                    showList();
                roomTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        showSelectedRoom(newValue);
                    }
                });
            }
        });
    }

    public void showSelectedRoom(Room room) {
        if (room.getAmount() == 4) {
            firstCustomerLabel.setText("1. ");
            secondCustomerLabel.setText("2. ");
            thirdCustomerLabel.setText("3. ");
            fourthCustomerLabel.setText("4. ");
            for (int i = 0; i < room.getCustomers().size(); i++) {
                if (i == 0)
                    firstCustomerLabel.setText("1. " + room.getCustomers().get(0).getName() + "   id: "+room.getCustomers().get(0).getId());
                else if (i == 1)
                    secondCustomerLabel.setText("2. " + room.getCustomers().get(1).getName() + "   id: "+room.getCustomers().get(1).getId());
                else if (i == 2)
                    thirdCustomerLabel.setText("3. " + room.getCustomers().get(2).getName() + "   id: "+room.getCustomers().get(2).getId());
                else
                    fourthCustomerLabel.setText("4. " + room.getCustomers().get(3).getName() + "   id: "+room.getCustomers().get(3).getId());
            }
        } else if (room.getAmount() == 2) {
            firstCustomerLabel.setText("1. ");
            secondCustomerLabel.setText("2. ");
            thirdCustomerLabel.setText("");
            fourthCustomerLabel.setText("");
            for (int i = 0; i < room.getCustomers().size(); i++) {
                if (i == 0)
                    firstCustomerLabel.setText("1. " + room.getCustomers().get(0).getName() + "   id: "+room.getCustomers().get(0).getId());
                else if (i == 1)
                    secondCustomerLabel.setText("2. " + room.getCustomers().get(1).getName() + "   id: "+room.getCustomers().get(1).getId());
            }
        }
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException {
        Button manageUserBtn = (Button) event.getSource();
        Stage stage = (Stage) manageUserBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException {
        Button manageRoomBtn = (Button) event.getSource();
        Stage stage = (Stage) manageRoomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageItemBtnOnAction(ActionEvent event) throws IOException {
        Button manageItemBtn = (Button) event.getSource();
        Stage stage = (Stage) manageItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageitempage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageItemPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleCheckItemBtnOnAction(ActionEvent event) throws IOException {
        Button checkItemBtn = (Button) event.getSource();
        Stage stage = (Stage) checkItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ItemListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleRoomBtnOnAction(ActionEvent event) throws IOException {
        Button roomBtn = (Button) event.getSource();
        Stage stage = (Stage) roomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        RoomListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void showList() {
        roomListTable = FXCollections.observableArrayList((roomsList.getAllRooms()));
        roomTable.setItems(roomListTable);
        TableColumn number = new TableColumn("Room number");
        TableColumn building = new TableColumn("Building");
        TableColumn floor = new TableColumn("Floor");
        TableColumn type = new TableColumn("Type");
        TableColumn size = new TableColumn("Size");

        number.setCellValueFactory(new PropertyValueFactory<Room, String>("roomNumber"));
        building.setCellValueFactory(new PropertyValueFactory<Room, String>("building"));
        floor.setCellValueFactory(new PropertyValueFactory<Room, String>("floor"));
        type.setCellValueFactory(new PropertyValueFactory<Room, String>("size"));
        size.setCellValueFactory(new PropertyValueFactory<Room, String>("amount"));

        number.setSortType(TableColumn.SortType.ASCENDING);
        roomTable.getColumns().addAll(number, building, floor, type, size);
        roomTable.getSortOrder().add(number);
    }
}
