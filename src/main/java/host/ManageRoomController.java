package host;

import account.*;
import data.AccountFileAccountDataInfo;
import data.AccountDataInfo;
import data.RoomDataInfo;
import data.RoomFileDataInfo;
import item.ItemList;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import login.*;
import room.Room;
import room.RoomList;

import java.io.IOException;

public class ManageRoomController {
    @FXML
    private Label currentAccountLabel;
    @FXML
    private ComboBox<String> userComboBox, sizeComboBox;
    @FXML
    private ComboBox<Integer> roomComboBox;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private Room room;
    private AccountDataInfo source;
    private RoomDataInfo roomSource;
    private AccountList accountsList;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML
    private TextField roomBox;
    private Alert alert;

    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                usersList.sortUsersList();
                for (UserAccount u : usersList.getUserAccounts())
                    if (u.getRoom() == -1)
                        userComboBox.getItems().add(u.getName() + " - " + u.getId());
                for (Room r : roomsList.getAllRooms())
                    if(roomsList.checkFull(r))
                        roomComboBox.getItems().add(r.getRoomNumber());
                sizeComboBox.getItems().add("2");
                sizeComboBox.getItems().add("4");
            }

        });
        alert = new Alert(Alert.AlertType.NONE);
        room = new Room();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException {
        Button manageUserBtn = (Button) event.getSource();
        Stage stage = (Stage) manageUserBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException {
        Button manageRoomBtn = (Button) event.getSource();
        Stage stage = (Stage) manageRoomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleCheckItemBtnOnAction(ActionEvent event) throws IOException {
        Button checkItemBtn = (Button) event.getSource();
        Stage stage = (Stage) checkItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ItemListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleAddBtnOnAction(ActionEvent event) throws IOException {
        try {
            Double.parseDouble(roomBox.getText());
            if (room.isValid(Integer.parseInt(roomBox.getText())))
            {
                if(!roomsList.checkExist(Integer.parseInt(roomBox.getText())))
                {
                    if(roomsList.arrangeRoom(Integer.parseInt(roomBox.getText())) == Integer.parseInt(roomBox.getText())) {
                        if (sizeComboBox.getValue() != null) {
                            String str = roomBox.getText();
                            char[] split = str.toCharArray();
                            char floor = split[1];
                            char building = split[0];
                            if (Integer.parseInt(sizeComboBox.getValue()) == 2) {
                                roomsList.addRoom(new Room(Integer.parseInt(roomBox.getText()), Integer.parseInt(sizeComboBox.getValue()), "Single room", building, floor));
                                roomSource = new RoomFileDataInfo("data", "RoomData.csv");
                                roomSource.setRoomData(roomsList);
                                alert.setAlertType(Alert.AlertType.INFORMATION);
                                alert.setContentText("Add room successful.");
                                alert.show();
                                Button addBtn = (Button) event.getSource();
                                Stage stage = (Stage) addBtn.getScene().getWindow();
                                FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
                                stage.setScene(new Scene(loader.load(), 1024, 768));
                                RoomListController account = loader.getController();
                                account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                                stage.show();
                            } else if (Integer.parseInt(sizeComboBox.getValue()) == 4) {
                                roomsList.addRoom(new Room(Integer.parseInt(roomBox.getText()), Integer.parseInt(sizeComboBox.getValue()), "Double room", building, floor));
                                roomSource = new RoomFileDataInfo("data", "RoomData.csv");
                                roomSource.setRoomData(roomsList);
                                alert.setAlertType(Alert.AlertType.INFORMATION);
                                alert.setContentText("Add room successful.");
                                alert.show();
                                Button addBtn = (Button) event.getSource();
                                Stage stage = (Stage) addBtn.getScene().getWindow();
                                FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
                                stage.setScene(new Scene(loader.load(), 1024, 768));
                                RoomListController account = loader.getController();
                                account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                                stage.show();
                            }
                        } else {
                            alert.setAlertType(Alert.AlertType.WARNING);
                            alert.setContentText("Please select size.");
                            alert.show();
                        }
                    }else
                    {
                        int roomNum =  roomsList.arrangeRoom(Integer.parseInt(roomBox.getText()));
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Please add room "+roomNum+" first.");
                        alert.show();
                    }
                }
                else
                {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("This room number is already existed.");
                    alert.show();
                }
            } else {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Room number is invalid.");
                alert.show();
            }
        } catch (NumberFormatException e) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Room number is invalid.");
            alert.show();
        }
    }
    public void handleAddToRoomBtnOnAction(ActionEvent event) throws IOException {
        try {
            String[] id = userComboBox.getValue().split("-");
            if (roomsList.checkFull(roomComboBox.getValue())) {
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Can't add more user to this room.");
            }
            else if (usersList.searchById(id[1].trim()) != null)
            {
                roomsList.addToRoom(usersList.searchById(id[1].trim()), roomComboBox.getValue());
                usersList.searchById(id[1].trim()).setRoom(roomComboBox.getValue());
                usersList.sortUsersList();
                source = new AccountFileAccountDataInfo("data", "Account.csv");
                source.setAccountData(accountsList);
                roomSource = new RoomFileDataInfo("data", "RoomData.csv");
                roomSource.setRoomData(roomsList);
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Add user to room " + roomComboBox.getValue() + " successful.");
                Button addToRoomBtn = (Button) event.getSource();
                Stage stage = (Stage) addToRoomBtn.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
                stage.setScene(new Scene(loader.load(), 1024, 768));
                RoomListController account = loader.getController();
                account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                stage.show();
            }
            alert.show();
        } catch (NullPointerException e) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please select account and room number.");
            alert.show();
        }
    }

    public void handleManageItemBtnOnAction(ActionEvent event) throws IOException {
        Button manageItemBtn = (Button) event.getSource();
        Stage stage = (Stage) manageItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageitempage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageItemPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleRoomBtnOnAction(ActionEvent event) throws IOException {
        Button roomBtn = (Button) event.getSource();
        Stage stage = (Stage) roomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        RoomListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
}
