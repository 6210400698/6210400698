package host;

import account.*;
import item.Document;
import item.Item;
import item.ItemList;
import item.Parcel;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import login.LoginController;
import room.RoomList;

import java.io.IOException;

public class HistoryPageController
{
    @FXML
    private TableView<Item> historyTable;
    private ObservableList<Item> historyListTable;

    @FXML private Label currentAccountLabel, statusLabel,senderLabel, receiverLabel
                        , sizeLabel, classificationLabel, deliveryLabel, trackLabel
                        , inBy, inDate, idLabel;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountList accountsList;
    private ItemList itemsList;
    public void setValue(ItemList itemsList,AccountList accountsList,RoomList roomsList,AdminAccount adminAccount,HostList hostsList,UserList usersList)
    {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML public void initialize()
    {
        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                if(!itemsList.getHistory().isEmpty())
                    showList();
                historyTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                {
                    if(newValue != null)
                    {
                        showSelectedItem(newValue);
                    }
                });
            }
        });
    }

    public void showSelectedItem(Item item)
    {
        statusLabel.setText("Status: Received by customer.");
        inBy.setText("In by: "+item.getHostReceive());
        inDate.setText("In date: "+item.getDateIn());
        sizeLabel.setText("Size: "+item.getSize());
        senderLabel.setText("Sender: "+item.getSenderName());
        receiverLabel.setText("Receiver: "+item.getReceiverName());
        idLabel.setText("User id: "+item.getId());
        classificationLabel.setText("");
        deliveryLabel.setText("");
        trackLabel.setText("");
        if(item instanceof Document)
        {
            classificationLabel.setText("Classification: " + ((Document) item).getClassification());
            deliveryLabel.setText("");
        }
        else if(item instanceof Parcel)
        {
            deliveryLabel.setText("Delivery service: "+((Parcel) item).getDeliveryService());
            trackLabel.setText("Tracking number: "+((Parcel) item).getTrackNumber());
        }

    }

    public void handleHostBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button hostBackBtn = (Button) event.getSource();
        Stage stage = (Stage) hostBackBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException
    {
        Button manageUserBtn = (Button) event.getSource();
        Stage stage = (Stage) manageUserBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException
    {
        Button manageRoomBtn = (Button) event.getSource();
        Stage stage = (Stage) manageRoomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleManageItemBtnOnAction(ActionEvent event) throws IOException
    {
        Button manageItemBtn = (Button) event.getSource();
        Stage stage = (Stage) manageItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageitempage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageItemPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleCheckItemBtnOnAction(ActionEvent event) throws IOException
    {
        Button checkItemBtn = (Button) event.getSource();
        Stage stage = (Stage) checkItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ItemListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostmainpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostMainPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
    public void handleRoomBtnOnAction(ActionEvent event) throws IOException
    {
        Button roomBtn = (Button) event.getSource();
        Stage stage = (Stage) roomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        RoomListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void showList()
    {
        historyListTable = FXCollections.observableArrayList((itemsList.getHistory()));
        historyTable.setItems(historyListTable);
        TableColumn type = new TableColumn("Type");
        TableColumn room = new TableColumn("Room");
        TableColumn out = new TableColumn("Date out");
        TableColumn outBy = new TableColumn("Out by");

        type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
        room.setCellValueFactory(new PropertyValueFactory<Item, String>("roomNumber"));
        out.setCellValueFactory(new PropertyValueFactory<Item, String>("dateOut"));
        outBy.setCellValueFactory(new PropertyValueFactory<Item, String>("userReceive"));
        outBy.setPrefWidth(140);

        historyTable.getColumns().addAll(type, room, out, outBy);
    }


}
