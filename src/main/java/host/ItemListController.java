package host;

import account.*;
import data.ItemDataInfo;
import data.ItemFileDataInfo;
import item.*;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import login.LoginController;
import room.RoomList;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ItemListController {
    @FXML
    private TableView<Item> itemTable;
    private ObservableList<Item> itemListTable;
    @FXML
    private TextField searchBox;
    @FXML
    private ImageView itemImage;

    @FXML
    private Label idLabel, currentAccountLabel, statusLabel, senderLabel, receiverLabel, sizeLabel, classificationLabel, deliveryLabel, trackLabel;
    @FXML
    private ComboBox<String> userComboBox;
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private AccountList accountsList;
    private Item selectedItem;
    private ItemDataInfo itemSource;
    private Alert alert;
    private ItemList itemsList;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML
    public void initialize() {
        alert = new Alert(Alert.AlertType.NONE);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                itemsList.removeItem();
                if (!itemsList.getItems().isEmpty())
                    showList();
                itemTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                {
                    if (newValue != null) {
                        selectedItem = newValue;
                        showSelectedItem(selectedItem);
                    }
                });
            }
        });
    }

    public void showSelectedItem(Item item) {
        statusLabel.setText("Status: Not receive by customer.");
        sizeLabel.setText("Size: " + item.getSize());
        senderLabel.setText("Sender: " + item.getSenderName());
        receiverLabel.setText("Receiver: " + item.getReceiverName());
        idLabel.setText("User id: "+item.getId());
        classificationLabel.setText("");
        deliveryLabel.setText("");
        trackLabel.setText("");
        if (item instanceof Letter) {
            itemImage.setImage(new Image("/image/letter.png"));
        } else if (item instanceof Document) {
            classificationLabel.setText("Classification: " + ((Document) item).getClassification());
            deliveryLabel.setText("");
            itemImage.setImage(new Image("/image/document.png"));
        } else if (item instanceof Parcel) {
            deliveryLabel.setText("Delivery service: " + ((Parcel) item).getDeliveryService());
            trackLabel.setText("Tracking number: " + ((Parcel) item).getTrackNumber());
            itemImage.setImage(new Image("/image/parcel.png"));
        }
        userComboBox.getItems().clear();
        for (UserAccount u : usersList.getUserAccounts()){
            if(u.checkSameItem(item,usersList.searchById(u.getId())))
                userComboBox.getItems().add(u.getName()+ " - " + u.getId());}
    }

    public void clearSelectedItem()
    {
        idLabel.setText("");
        statusLabel.setText("");
        sizeLabel.setText("");
        senderLabel.setText("");
        receiverLabel.setText("");
        classificationLabel.setText("");
        deliveryLabel.setText("");
        trackLabel.setText("");
    }

    public void handleHostBackBtnOnAction(ActionEvent event) throws IOException {
        Button hostBackBtn = (Button) event.getSource();
        Stage stage = (Stage) hostBackBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException {
        Button manageUserBtn = (Button) event.getSource();
        Stage stage = (Stage) manageUserBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException {
        Button manageRoomBtn = (Button) event.getSource();
        Stage stage = (Stage) manageRoomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageItemBtnOnAction(ActionEvent event) throws IOException {
        Button manageItemBtn = (Button) event.getSource();
        Stage stage = (Stage) manageItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageitempage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageItemPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleCheckItemBtnOnAction(ActionEvent event) throws IOException {
        Button checkItemBtn = (Button) event.getSource();
        Stage stage = (Stage) checkItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ItemListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button backBtn = (Button) event.getSource();
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostmainpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostMainPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleRoomBtnOnAction(ActionEvent event) throws IOException {
        Button roomBtn = (Button) event.getSource();
        Stage stage = (Stage) roomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        RoomListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleHistoryBtnOnAction(ActionEvent event) throws IOException {
        Button historyBtn = (Button) event.getSource();
        Stage stage = (Stage) historyBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/historylist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HistoryPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void showList() {
        itemListTable = FXCollections.observableArrayList((itemsList.getItems()));
        itemTable.setItems(itemListTable);
        TableColumn type = new TableColumn("Type");
        TableColumn room = new TableColumn("Room");
        TableColumn in = new TableColumn("Date in");
        TableColumn inBy = new TableColumn("In by");

        type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
        room.setCellValueFactory(new PropertyValueFactory<Item, String>("roomNumber"));
        in.setCellValueFactory(new PropertyValueFactory<Item, String>("dateIn"));
        inBy.setCellValueFactory(new PropertyValueFactory<Item, String>("hostReceive"));
        inBy.setPrefWidth(140);

        in.setSortType(TableColumn.SortType.DESCENDING);
        itemTable.getColumns().addAll(type, room, in, inBy);
        itemTable.getSortOrder().add(in);

        ObservableList data = itemTable.getItems();
        searchBox.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) ->
        {
            if (oldValue != null && (newValue.length() < oldValue.length())) {
                itemTable.setItems(data);
            }
            String search = newValue.toLowerCase();
            ObservableList<Item> found = FXCollections.observableArrayList();
            long count = itemTable.getColumns().stream().count();
            for (int i = 0; i < itemTable.getItems().size(); i++) {
                for (int j = 0; j < count; j++) {
                    String left = "" + itemTable.getColumns().get(j).getCellData(i);
                    if (left.toLowerCase().contains(search)) {
                        found.add(itemTable.getItems().get(i));
                        break;
                    }
                }
            }
            itemTable.setItems(found);
        });
    }

    public void handleRoomAscendBtnOnAction(ActionEvent event) {
        clearSelectedItem();
        if (!itemsList.getItems().isEmpty()) {
            itemTable.getColumns().clear();
            itemListTable = FXCollections.observableArrayList((itemsList.getItems()));
            itemTable.setItems(itemListTable);
            TableColumn type = new TableColumn("Type");
            TableColumn room = new TableColumn("Room");
            TableColumn in = new TableColumn("Date in");
            TableColumn inBy = new TableColumn("In by");

            type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
            room.setCellValueFactory(new PropertyValueFactory<Item, String>("roomNumber"));
            in.setCellValueFactory(new PropertyValueFactory<Item, String>("dateIn"));
            inBy.setCellValueFactory(new PropertyValueFactory<Item, String>("hostReceive"));
            inBy.setPrefWidth(140);

            room.setSortType(TableColumn.SortType.ASCENDING);
            itemTable.getColumns().addAll(type, room, in, inBy);
            itemTable.getSortOrder().add(room);
        }
    }

    public void handleRoomDescendBtnOnAction(ActionEvent event) {
        clearSelectedItem();
        if (!itemsList.getItems().isEmpty()) {
            itemTable.getColumns().clear();
            itemListTable = FXCollections.observableArrayList((itemsList.getItems()));
            itemTable.setItems(itemListTable);
            TableColumn type = new TableColumn("Type");
            TableColumn room = new TableColumn("Room");
            TableColumn in = new TableColumn("Date in");
            TableColumn inBy = new TableColumn("In by");

            type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
            room.setCellValueFactory(new PropertyValueFactory<Item, String>("roomNumber"));
            in.setCellValueFactory(new PropertyValueFactory<Item, String>("dateIn"));
            inBy.setCellValueFactory(new PropertyValueFactory<Item, String>("hostReceive"));
            inBy.setPrefWidth(140);

            room.setSortType(TableColumn.SortType.DESCENDING);
            itemTable.getColumns().addAll(type, room, in, inBy);
            itemTable.getSortOrder().add(room);
        }
    }

    public void handleNewestBtnOnAction(ActionEvent event) {
        clearSelectedItem();
        if (!itemsList.getItems().isEmpty()) {
            itemTable.getColumns().clear();
            itemListTable = FXCollections.observableArrayList((itemsList.getItems()));
            itemTable.setItems(itemListTable);
            TableColumn type = new TableColumn("Type");
            TableColumn room = new TableColumn("Room");
            TableColumn in = new TableColumn("Date in");
            TableColumn inBy = new TableColumn("In by");

            type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
            room.setCellValueFactory(new PropertyValueFactory<Item, String>("roomNumber"));
            in.setCellValueFactory(new PropertyValueFactory<Item, String>("dateIn"));
            inBy.setCellValueFactory(new PropertyValueFactory<Item, String>("hostReceive"));
            inBy.setPrefWidth(140);

            in.setSortType(TableColumn.SortType.DESCENDING);
            itemTable.getColumns().addAll(type, room, in, inBy);
            itemTable.getSortOrder().add(in);
        }
    }

    public void handleOldestBtnOnAction(ActionEvent event) throws IOException {
        clearSelectedItem();
        if (!itemsList.getItems().isEmpty()) {
            itemTable.getColumns().clear();
            itemListTable = FXCollections.observableArrayList((itemsList.getItems()));
            itemTable.setItems(itemListTable);
            TableColumn type = new TableColumn("Type");
            TableColumn room = new TableColumn("Room");
            TableColumn in = new TableColumn("Date in");
            TableColumn inBy = new TableColumn("In by");

            type.setCellValueFactory(new PropertyValueFactory<Item, String>("type"));
            room.setCellValueFactory(new PropertyValueFactory<Item, String>("roomNumber"));
            in.setCellValueFactory(new PropertyValueFactory<Item, String>("dateIn"));
            inBy.setCellValueFactory(new PropertyValueFactory<Item, String>("hostReceive"));
            inBy.setPrefWidth(140);

            in.setSortType(TableColumn.SortType.ASCENDING);
            itemTable.getColumns().addAll(type, room, in, inBy);
            itemTable.getSortOrder().add(in);
        }
    }

    public void handleReceiveBtnOnAction(ActionEvent event) throws IOException {
        try {
            String[] user = userComboBox.getValue().split("-");
            String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
            if (itemsList.receiveItem(usersList.searchById(user[1].trim()), timeStamp, selectedItem))
            {
                itemSource = new ItemFileDataInfo("data", "ItemData.csv");
                itemSource.setItemData(itemsList);
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Receive item successful.");
                alert.show();
                userComboBox.getItems().clear();
                itemImage.setImage(null);
                usersList.searchById(user[1].trim()).removeReceived(selectedItem);
                itemSource = new ItemFileDataInfo("data", "ItemData.csv");
                itemSource.setItemData(itemsList);
                clearSelectedItem();
                itemTable.getColumns().clear();
                showList();
            } else {
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("User is not in a room.");
                alert.show();
            }
        } catch (NullPointerException e) {
            alert.setAlertType(Alert.AlertType.INFORMATION);
            alert.setContentText("There's no selected item or receiver.");
            alert.show();
        }
    }
}
