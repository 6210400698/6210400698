package host;

import account.*;
import data.ItemDataInfo;
import data.ItemFileDataInfo;
import item.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import login.*;
import room.Room;
import room.RoomList;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;

public class ManageItemPageController {
    private AdminAccount adminAccount;
    private HostList hostsList;
    private UserList usersList;
    private RoomList roomsList;
    private Account accounts;
    private AccountList accountsList;
    private ItemList itemsList;
    private ItemDataInfo itemSource;
    private Alert alert;

    public void setValue(ItemList itemsList, AccountList accountsList, RoomList roomsList, AdminAccount adminAccount, HostList hostsList, UserList usersList) {
        this.itemsList = itemsList;
        this.accountsList = accountsList;
        this.roomsList = roomsList;
        this.adminAccount = adminAccount;
        this.hostsList = hostsList;
        this.usersList = usersList;
    }

    @FXML
    private ComboBox<String> letterComboBox, documentComboBox, parcelComboBox, classificationComboBox;
    @FXML
    private Label currentAccountLabel;
    @FXML
    private TextField senderBox, sizeBox;
    @FXML
    private TextField senderDocBox, sizeDocBox;
    @FXML
    private TextField senderParcelBox, sizeParcelBox, deliveryServiceBox, trackingNumberBox;

    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                usersList.sortUsersList();
                for (UserAccount u : usersList.getUserAccounts())
                    if (u.getRoom() != -1) {
                        letterComboBox.getItems().add(u.getName() + " - " + u.getId() + " - " + u.getRoom());
                        documentComboBox.getItems().add(u.getName() + " - " + u.getId() + " - " + u.getRoom());
                        parcelComboBox.getItems().add(u.getName() + " - " + u.getId() + " - " + u.getRoom());
                    }
                classificationComboBox.getItems().add("Secret");
                classificationComboBox.getItems().add("Quickest");
                classificationComboBox.getItems().add("Normal");
                currentAccountLabel.setText(accountsList.getCurrentAccount().getName());
                alert = new Alert(Alert.AlertType.NONE);
            }
        });
        accounts = new Account();
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException {
        Button changePasswordBtn = (Button) event.getSource();
        Stage stage = (Stage) changePasswordBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException {
        Button manageUserBtn = (Button) event.getSource();
        Stage stage = (Stage) manageUserBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException {
        Button manageRoomBtn = (Button) event.getSource();
        Stage stage = (Stage) manageRoomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleManageItemBtnOnAction(ActionEvent event) throws IOException {
        Button manageItemBtn = (Button) event.getSource();
        Stage stage = (Stage) manageItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageitempage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageItemPageController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleHostBackBtnOnAction(ActionEvent event) throws IOException {
        Button hostBackBtn = (Button) event.getSource();
        Stage stage = (Stage) hostBackBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/loginpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        LoginController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleAddLetterBtnOnAction(ActionEvent event) throws IOException {
        if (senderBox.getText().equals("") || sizeBox.getText().equals("")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        } else if (letterComboBox.getValue() == null) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please select receiver.");
            alert.show();
        } else {
            String[] user = letterComboBox.getValue().split("-");
            try {
                Double.parseDouble(senderBox.getText());
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Name can't be number.");
                alert.show();
            } catch (NumberFormatException n) {
                try {
                    String data[] = sizeBox.getText().split("x");
                    Double.parseDouble(data[0]);
                    Double.parseDouble(data[1]);
                    Double.parseDouble(data[2]);
                    if (Double.parseDouble(data[0]) < 0 || Double.parseDouble(data[1]) < 0 || Double.parseDouble(data[2]) < 0) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Size can't be negative.");
                        alert.show();
                        return;
                    }
                    else if (!accounts.isValidName(senderBox.getText())) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Sender name is invalid.");
                        alert.show();
                    }
                    else {
                        Letter item = new Letter("Letter", senderBox.getText(), user[0].trim(), Integer.parseInt(user[2].trim())
                                , sizeBox.getText(), user[1].trim());
                        item.setHostReceive(accountsList.getCurrentAccount().getName());
                        String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                        item.setDateIn(timeStamp);
                        itemsList.addItem(item);
                        usersList.searchById(user[1].trim()).getUserInbox().add(item);
                        itemSource = new ItemFileDataInfo("data", "ItemData.csv");
                        itemSource.setItemData(itemsList);
                        Button addLetterBtn = (Button) event.getSource();
                        Stage stage = (Stage) addLetterBtn.getScene().getWindow();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
                        stage.setScene(new Scene(loader.load(), 1024, 768));
                        ItemListController account = loader.getController();
                        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                        stage.show();
                    }
                } catch (NumberFormatException e) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Size format is incorrect.");
                    alert.show();
                } catch (ArrayIndexOutOfBoundsException e) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Size format is incorrect.");
                    alert.show();
                }
            }
        }
    }

    public void handleAddDocumentBtnOnAction(ActionEvent event) throws IOException {
        if (senderDocBox.getText().equals("") || sizeDocBox.getText().equals("")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        } else if (documentComboBox.getValue() == null) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please select receiver.");
            alert.show();
        } else if (classificationComboBox.getValue() == null) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please select classification.");
            alert.show();
        } else {
            String[] user = documentComboBox.getValue().split("-");
            try {
                Double.parseDouble(senderDocBox.getText());
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Name can't be number.");
                alert.show();
            } catch (NumberFormatException n) {
                try {
                    String data[] = sizeDocBox.getText().split("x");
                    Double.parseDouble(data[0]);
                    Double.parseDouble(data[1]);
                    Double.parseDouble(data[2]);
                    if (Double.parseDouble(data[0]) < 0 || Double.parseDouble(data[1]) < 0 || Double.parseDouble(data[2]) < 0) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Size can't be negative.");
                        alert.show();
                        return;
                    }
                    else if (!accounts.isValidName(senderDocBox.getText())) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Sender name is invalid.");
                        alert.show();
                    }
                    else {
                        Document item = new Document("Document", senderDocBox.getText(), user[0].trim()
                                , Integer.parseInt(user[2].trim()), sizeDocBox.getText()
                                , classificationComboBox.getValue(), user[1].trim());
                        item.setHostReceive(accountsList.getCurrentAccount().getName());
                        String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                        item.setDateIn(timeStamp);
                        itemsList.addItem(item);
                        usersList.searchById(user[1].trim()).getUserInbox().add(item);
                        itemSource = new ItemFileDataInfo("data", "ItemData.csv");
                        itemSource.setItemData(itemsList);
                        Button addDocumentBtn = (Button) event.getSource();
                        Stage stage = (Stage) addDocumentBtn.getScene().getWindow();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
                        stage.setScene(new Scene(loader.load(), 1024, 768));
                        ItemListController account = loader.getController();
                        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                        stage.show();
                    }
                } catch (NumberFormatException e) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Size format is incorrect.");
                    alert.show();
                } catch (ArrayIndexOutOfBoundsException e) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Size format is incorrect.");
                    alert.show();
                }
            }
        }
    }

    public void handleAddParcelBtnOnAction(ActionEvent event) throws IOException {
        if (senderParcelBox.getText().equals("") || sizeParcelBox.getText().equals("")
                || deliveryServiceBox.getText().equals("") || trackingNumberBox.getText().equals("")) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Field can't be empty.");
            alert.show();
        } else if (parcelComboBox.getValue() == null) {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Please select receiver.");
            alert.show();
        } else {
            String[] user = parcelComboBox.getValue().split("-");
            try {
                Double.parseDouble(senderParcelBox.getText());
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setContentText("Name can't be number.");
                alert.show();
            } catch (NumberFormatException n) {
                try {
                    String data[] = sizeParcelBox.getText().split("x");
                    Double.parseDouble(data[0]);
                    Double.parseDouble(data[1]);
                    Double.parseDouble(data[2]);
                    if (Double.parseDouble(data[0]) < 0 || Double.parseDouble(data[1]) < 0 || Double.parseDouble(data[2]) < 0) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Size can't be negative.");
                        alert.show();
                        return;
                    }
                    if (!accounts.isValidName(senderParcelBox.getText())) {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setContentText("Sender name is invalid.");
                        alert.show();
                    }
                    else {
                        Parcel item = new Parcel("Parcel", senderParcelBox.getText(), user[0].trim()
                                , Integer.parseInt(user[2].trim()), sizeParcelBox.getText()
                                , deliveryServiceBox.getText(), trackingNumberBox.getText(), user[1].trim());
                        item.setHostReceive(accountsList.getCurrentAccount().getName());
                        String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                        item.setDateIn(timeStamp);
                        itemsList.addItem(item);
                        usersList.searchById(user[1].trim()).getUserInbox().add(item);
                        itemSource = new ItemFileDataInfo("data", "ItemData.csv");
                        itemSource.setItemData(itemsList);
                        Button addParcelBtn = (Button) event.getSource();
                        Stage stage = (Stage) addParcelBtn.getScene().getWindow();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
                        stage.setScene(new Scene(loader.load(), 1024, 768));
                        ItemListController account = loader.getController();
                        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
                        stage.show();
                    }
                } catch (NumberFormatException e) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Size format is incorrect.");
                    alert.show();
                } catch (ArrayIndexOutOfBoundsException e) {
                    alert.setAlertType(Alert.AlertType.WARNING);
                    alert.setContentText("Size format is incorrect.");
                    alert.show();
                }
            }
        }
    }

    public void handleCheckItemBtnOnAction(ActionEvent event) throws IOException {
        Button checkItemBtn = (Button) event.getSource();
        Stage stage = (Stage) checkItemBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/itemlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ItemListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }

    public void handleRoomBtnOnAction(ActionEvent event) throws IOException {
        Button roomBtn = (Button) event.getSource();
        Stage stage = (Stage) roomBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/roomlist.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        RoomListController account = loader.getController();
        account.setValue(itemsList, accountsList, roomsList, adminAccount, hostsList, usersList);
        stage.show();
    }
}
